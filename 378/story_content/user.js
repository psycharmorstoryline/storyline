
if (!String.prototype.includes) {
    String.prototype.includes = function(search, start) {
        if (typeof start !== 'number') {
            start = 0;
        }

        if (start + search.length > this.length) {
            return false;
        } else {
            return this.indexOf(search, start) !== -1;
        }
    };
}

//JS Code Insertion

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var actor = JSON.parse(getParameterByName('actor'));
var baseUrl = getParameterByName('base_url');
var nonce = getParameterByName('nonce');
var email = actor.mbox[0].replace('mailto:', '');
var postId = getParameterByName('auth').replace('LearnDashId', '');

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
		// Delete document
		if (xhttp.responseText === 'false') {
			document.body.innerHTML = '';
		}
	}

    if (this.readyState == 4 && this.status != 200) {
		document.body.innerHTML = '';
	}
};

xhttp.open('GET', baseUrl + '/wp-json/uncanny_reporting/v1/auth/?email=' + email + '&nonce=' + nonce + '&postId=' + postId, true);
xhttp.send();
function ExecuteScript(strId)
{
  switch (strId)
  {
      case "6kBKZsZ9EET":
        Script1();
        break;
  }
}

function Script1()
{
  parent.submitLearnDashTopicComplete();
}

